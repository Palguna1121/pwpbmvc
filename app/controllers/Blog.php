<?php
   class Blog extends Controller {
       public function index () {
           $data["judul"] = "Blog";
           $data["nama"] = "palguna";
           $data["blog"] = "gajelas";
           $data['all-blog'] = $this->model("Blog_model")->getAllBlog();

           $this->view("templates/header", $data);
           $this->view("blog/index", $data);
           $this->view("templates/footer");
       }
       public function show($id) {
        $data["judul"] = "Blog";
        $data["nama"] = "palguna";
        $data["singleBlog"] = $this->model("Blog_model")->get($id);
        $this->view("templates/header", $data);
        $this->view("blog/show", $data);
        $this->view("templates/footer");
        
       }

       public function create() {
        $data["judul"] = "create";
        $this->view("templates/header", $data);
        $this->view("blog/create");
        $this->view("templates/footer");
       }

       public function update() {
        $data["judul"] = "update";
        $this->view("templates/header", $data);
        $this->view("blog/update");
        $this->view("templates/footer");
       }

   }
