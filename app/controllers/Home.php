<?php

// class Home extends Controller 
// {
//   public function index() 
//   {
//       $data = [
//         "title" => 'home',
//         "nama" => 'anjim'
//       ];
//       // $data['judul'] = 'Home';
//       $this->view('home/index', $data);
//   }
// }

class Home extends Controller {

  public function __construct()
    {
        if(! isset($_SESSION['user_login']))
        {
            //belum login maka
            return redirect("user/login");
        }
    }

  public function index() 
  {
    $data["judul"] = "Home";
    $data['nama'] = "palguna";
 
    $this->view("templates/header", $data);
    $this->view("home/index", $data);
    $this->view("templates/footer");
  }

}