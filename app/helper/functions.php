<?php

function url(string $givenUrl): string
{
    if($givenUrl[0] !== '/' && BASE_URL[strlen(BASE_URL) - 1] !== '/'){
        $givenUrl = '/' . $givenUrl;
    }
    return BASE_URL . $givenUrl;
}

function redirect(string $path, ?array $message = null)
{
    if($message !== null)
    {
        foreach($message as $key => $value)
        {
            $_SESSION[$key] = $value;
        }    
    }
    if($path[0] == '/'){
        ltrim($path, '/');
    }
    $path = url($path);
    header("Location: $path");
    die;
}

function close_all_session_except(?string ...$sessionKeys)
{
    $enableSessionKeys = array_keys($_SESSION);

    foreach($enableSessionKeys as $single)
    {
        if(!in_array($single, $sessionKeys))
        {
            unset($_SESSION[$single]);
        }
    }

}