<?php

class Hash 
{
    PUBLIC CONST SECRET_KEY = "qwertyuioplkjhgfdsazxcvbnm";
    
    public static function make(string $raw_password) :string
    {
        return md5($raw_password) . self::SECRET_KEY;
    }

    public static function check(string $raw_password, string $hashed_password) :bool
    {
        return md5($raw_password) . self::SECRET_KEY === $hashed_password;
    }
}